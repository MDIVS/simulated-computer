## [Simulated Computer](https://gitlab.com/MDIVS/simulated-computer)

_A virtual computer architecture made by [Maicon Santos](https://www.facebook.com/MDIVSbr) with logisim._

---

## ROM program architecture
The ROM will accept a 5bit sequence as address and 8bit sequence as values, that is:

- First 2 bits: RAM address
- Next 2 bits: Register address
- Next 4 bits: Operator code

> The **RAM Address** is what position on the RAM will be writed or readed;
> 
> The **Register Address** is what register in the *Central Processor* (we will talk about it) will be allocated to get or set values from or to RAM;
>
> The **Operator Code** is a predetermined sequence that allows the *Instruction Decoder* to make a specific operation.

---

> ### **Operator codes**:
>
> | Name    | Code | Description    |
> | ------- | -----| -------------- |
> | ADD     | 1001 | Ra <- Rx+Ry    |
> | HLT     | 1000 | Count stop     |
> | MOV Ry  | 1011 | [Rx] ← [Ry]    |
> | MOV RA  | 1100 | [Rx] ← [RA]    |
> | RAM out | 1101 | [Rx] ← RAM out |
> | RAM in  | 1110 | RAM in ← [Rx]  |